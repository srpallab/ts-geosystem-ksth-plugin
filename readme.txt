 Plugin Name:       TS Geosystem CC
 Plugin URI:        https://techub.kazispin.com/
 Description:       Simple Equipment Informations Store. 
 Version:           0.0.1
 Requires at least: 5.8.1
 Requires PHP:      7.4
 Author:            Shafiqur Rahman
 Author URI:        https://srpallab.com
 Text Domain:       ksth-ts-geosystem
 License:           GPLv3.0
 License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 Update URI:        https://techub.kazispin.com/
