<?php
/**
 * Plugin Name
 *
 * @package           PluginPackage
 * @author            Shafiqur Rahman
 * @copyright         2021 KaziSpin TecHub
 * @license           GPL-3.0
 *
 * @wordpress-plugin
 * Plugin Name:       TS Geosystem CC
 * Plugin URI:        https://techub.kazispin.com/
 * Description:       Simple Equipment Informations Store. 
 * Version:           0.0.1
 * Requires at least: 5.8.1
 * Requires PHP:      7.4
 * Author:            Shafiqur Rahman
 * Author URI:        https://srpallab.com
 * Text Domain:       ksth-ts-geosystem
 * License:           GPLv3.0
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Update URI:        https://techub.kazispin.com/
 */

// This if statement prevent from accessing this class from outside
if ( !defined('ABSPATH') ) {
    exit;
}

require_once __DIR__ . '/vendor/autoload.php';


class TsGeosystemCC {
    // Plugin version
    const VERSION = '0.0.1';
    
    public function __construct(){
        $this->defineConstants();
        register_activation_hook( __FILE__, [$this, 'activate']);
        new TSGEOSYSTEM\KSTH\Assets();

        if ( defined('DOING_AJAX') && DOING_AJAX ) {
            new TSGEOSYSTEM\KSTH\Ajax();
        }
        
        if ( is_admin() ) {
            new TSGEOSYSTEM\KSTH\Admin();
        } else {
            new TSGEOSYSTEM\KSTH\Frontend();
        }
    }


    // Singleton Method
    public static function init(){

        static $instance = false;

        if (!$instance) {
            $instance = new self();
        }

        return $instance;
    }

    // Define all constents
    public function defineConstants() {
        define('TS_GEOSYSTEM_VERSION', self::VERSION);
        define('TS_GEOSYSTEM_FILE', __FILE__);
        define('TS_GEOSYSTEM_PATH', __DIR__);
        define('TS_GEOSYSTEM_URL', plugins_url( '', TS_GEOSYSTEM_FILE ) );
        define('TS_GEOSYSTEM_ASSETS', TS_GEOSYSTEM_URL . '/assets');
    }

        // Acivation Works
    public function activate() {
        $installerObj = new TSGEOSYSTEM\KSTH\Installer();
        $installerObj->run();
    }

}

function tsGeosystemInstance(){
    return TsGeosystemCC::init();
}

tsGeosystemInstance();
