;(function($){
    // console.log($);
    $('#ts-geosystem-ksth-tracking-from form').on('submit', function(e){
	// console.log(e);
	e.preventDefault();
	const data = $(this).serialize();
				// console.log(data);
				// console.log(tsGeosystem.ajaxurl);
	$.post(tsGeosystem.ajaxurl, data, function(response){
	    if (response.success && response.data !== 'null') {
		$('#tracking-number').val('');
		$('#serial-number').val('');
		const res = JSON.parse(response.data);
		const div = $('#ts-geosystem-ksth-data-show');
		div.empty();
		// console.log($div);
		let table = document.createElement('table');
		let thead = document.createElement('thead');
		let tbody = document.createElement('tbody');
		let tr1 = document.createElement('tr');
		let tr2 = document.createElement('tr');
					let trackingNo = document.createElement('th');
					let equipment = document.createElement('th');
					let serialNumber = document.createElement('th');
					let accuracy = document.createElement('th');
					let manufacturer = document.createElement('th');
					let companyName = document.createElement('th');
					let validUntil = document.createElement('th');

					trackingNo.innerHTML = "Tracking No";
					equipment.innerHTML = "Equipment";
					serialNumber.innerHTML = "Serial Number";
					accuracy.innerHTML = "Accuracy";		
					manufacturer.innerHTML = "Manufacturer";
					companyName.innerHTML = "Company Name";
					validUntil.innerHTML = "Validity";

					tr1.append(trackingNo);
					tr1.append(equipment);
					tr1.append(serialNumber);
					tr1.append(accuracy);
					tr1.append(manufacturer);
					tr1.append(companyName);
					tr1.append(validUntil);

		thead.append(tr1);
		table.append(thead);
		
					let r1 = document.createElement('td');
					r1.innerHTML = res.tracking_no;
					let r2 = document.createElement('td');
					r2.innerHTML = res.equipment;
					let r3 = document.createElement('td');
					r3.innerHTML = res.serial_number;
					let r4 = document.createElement('td');
					r4.innerHTML = res.accuracy;
					let r5 = document.createElement('td');
					r5.innerHTML = res.manufacturer;
					let r6 = document.createElement('td');
					r6.innerHTML = res.company_name;
					let r7 = document.createElement('td');
					r7.innerHTML = res.valid_until;

		tr2.append(r1);
		tr2.append(r2);
		tr2.append(r3);
		tr2.append(r4);
		tr2.append(r5);
		tr2.append(r6);
		tr2.append(r7);
		
		tbody.append(tr2);
		table.append(tbody);
		
		div.append(table);
	    } else {
		// console.log(response);
		$('#tracking-number').value = '';
		$('#serial-number').value = '';
		
		const div = $('#ts-geosystem-ksth-data-show');
		div.empty();
		let h1 = document.createElement('h1');
		h1.innerHTML = "No Data Found";
		div.append(h1);
		
	    }
	}).fail(function(error){
	    alert(tsGeosystem.error);
	});
    });
})(jQuery);
