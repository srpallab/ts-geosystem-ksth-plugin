<?php

namespace TSGEOSYSTEM\KSTH;


class Admin {
    function __construct(){
        $addressBookObj = new Admin\Addressbook();
        new Admin\Menu( $addressBookObj );
        $this->dispatchAction( $addressBookObj );
    }

    public function dispatchAction( $addressBookObj ) {
        add_action( 'admin_init', [$addressBookObj, 'formHandler' ]);
        add_action( 'admin_post_ksth-delete-equipment', [$addressBookObj, 'deleteequipment' ]);
    }
}
