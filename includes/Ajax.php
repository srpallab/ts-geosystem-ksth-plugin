<?php

namespace TSGEOSYSTEM\KSTH;

class Ajax {

    public function __construct(){
        add_action('wp_ajax_nopriv_ts_geosystem_ksth_tracking', [$this, submitAjax]);
    }

    public function submitAjax() {
        // check_ajax_referer('wp-ac-enquiry-form');
        if ( !wp_verify_nonce( $_REQUEST['_wpnonce'], 'wp-ac-enquiry-form' )) {
            // wp_die( 'Are you cheating on nonce?' );
            wp_send_json_error('Verification Failed');
            // die();
        }
        
        //print_r($_REQUEST);
        $trackingNo = empty(
            $_REQUEST['tracking-number']
        ) ? '' : sanitize_text_field( $_REQUEST['tracking-number'] );

        $serialNo = empty(
            $_REQUEST['serial-number']
        ) ? '' : sanitize_text_field($_REQUEST['serial-number']);
        //print_r([$trackingNo, $serialNo]);
        //die();
        

        $equipmentInformations = ksthTsGetEquipmentWithTC($trackingNo, $serialNo);

        // print_r([$equipmentInformations]);
        // die();
        wp_send_json_success(wp_json_encode($equipmentInformations));
        
    }
    
}
