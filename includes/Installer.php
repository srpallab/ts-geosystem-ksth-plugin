<?php

namespace TSGEOSYSTEM\KSTH;

class Installer {
    function run(){
        $this->addVersion();
        $this->createTables();
    }
    
    // adding version into wp_options table
    public function addVersion() {
        $installed = get_option( 'ksth_ts_geosystem_installed' );
        if ( !$installed ) {
            update_option( 'ksth_ts_geosystem_installed', time() );
        }
        update_option( 'ksth_ts_geosystem_version', TS_GEOSYSTEM_VERSION);  
    }


    // Create necessay Tables
    public function createTables() {
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();
        
        $schema  = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}ksth_ts_geosystem_informations` (";
        $schema .= " `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,";
        $schema .= " `tracking_no` varchar(150) NOT NULL DEFAULT '',";
        $schema .= " `equipment` varchar(150) NOT NULL DEFAULT '',";
        $schema .= " `serial_number` varchar(150) NOT NULL DEFAULT '',";
        $schema .= " `accuracy` varchar(150) NOT NULL DEFAULT '',";
        $schema .= " `manufacturer` varchar(150) NOT NULL DEFAULT '',";
        $schema .= " `company_name` varchar(150) NOT NULL DEFAULT '',";
        $schema .= " `valid_until` date NOT NULL,";
        $schema .= " `created_by` bigint(20) unsigned NOT NULL,";
        $schema .= " `created_at` datetime NOT NULL,";
        $schema .= " PRIMARY KEY (`id`)";
        $schema .= ") $charset_collate;";

        // echo $schema;
        // die();

        if ( !function_exists( 'dbDelta' ) ) {
            require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        }

        dbDelta( $schema );
    }
}
