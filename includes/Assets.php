<?php

namespace TSGEOSYSTEM\KSTH;


class Assets {

    public function __construct(){
        add_action('wp_enqueue_scripts', [$this, 'enqueueAssets']);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAssets']);
    }

    public function getScripts() {
        return [
            'ts-geosystem-script' => [
                'src' => TS_GEOSYSTEM_ASSETS . '/js/frontend.js',
                'version' => filemtime(TS_GEOSYSTEM_PATH . '/assets/js/frontend.js'),
                'deps' => ['jquery']
            ],
            'ts-geosystem-admin-script' => [
                'src' => TS_GEOSYSTEM_ASSETS . '/js/admin.js',
                'version' => filemtime(TS_GEOSYSTEM_PATH . '/assets/js/admin.js'),
                'deps' => ['jquery']
            ]
        ];
    }
    
    public function getStyles() {
        return [
            'ts-geosystem-style' => [
                'src' => TS_GEOSYSTEM_ASSETS . '/css/frontend.css',
                'version' => filemtime(TS_GEOSYSTEM_PATH . '/assets/css/frontend.css')
            ],
            'ts-geosystem-admin-style' => [
                'src' => TS_GEOSYSTEM_ASSETS . '/css/admin.css',
                'version' => filemtime(TS_GEOSYSTEM_PATH . '/assets/css/admin.css')
            ]
        ];
    }
    
    public function enqueueAssets() {

        $scripts = $this->getScripts();
        foreach ($scripts as $handler => $script) {
            $deps = isset($script['deps']) ? $script['deps'] : false;
            // slug, url, dependency, version, footer 
            wp_register_script( $handler, $script['src'], $deps, $script['version'], true);
        }

        $styles = $this->getStyles();
        foreach ($styles as $handler => $style) {
            $deps = isset($style['deps']) ? $style['deps'] : false;
            wp_register_style( $handler, $style['src'], $deps, $style['version']);

        }
        wp_localize_script( 'ts-geosystem-script', 'tsGeosystem' , [
            'ajaxurl' => admin_url('admin-ajax.php'),
            'error' => __('Something Went Wrong!', 'ksth-ts-geosystem')
        ]);
        // wp_enqueue_script('ts-geosystem-script');
        // wp_enqueue_style('ts-geosystem-style');
        
    }
}
