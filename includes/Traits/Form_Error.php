<?php

namespace TSGEOSYSTEM\KSTH\Traits;


trait Form_Error {
    public $errors = [];

    public function hasError( $key ) {
        return isset( $this->errors[$key] ) ? true : false;
    }

    public function getError( $key ) {
        if ( isset ($this->errors[$key]) ) {
            return $this->errors[$key];
        }

        return false;
    }
}
