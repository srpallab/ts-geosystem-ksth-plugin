<?php

function ksthAcInsertAddress( $args = []) {
    global $wpdb;

    if (
        empty($args['tracking_no']) &&
        empty($args['equipment']) &&
        empty($args['serial_number']) &&
        empty($args['accuracy'] )&&
        empty($args['manufacturer']) &&
        empty($args['company_name']) &&
        empty($args['valid_until'])
    ) {

        return new \WP_Error( 'fields are empty', __( 'Fields are empty', 'ksth-ts-geosystem' ) );
    }

    $defalts = [
         'tracking_no' => '',
         'equipment' => '',
         'serial_number' => '',
         'accuracy' => '',
         'manufacturer' => '',
         'company_name' => '',
         'valid_until' => '',
         'created_by' => get_current_user_id(),
         'created_at' => current_time( 'mysql' )
    ];

    $data = wp_parse_args( $args, $defalts );

    if ( isset ($data['id']) ) {
        $id = $data['id'];
        unset($data['id']);
        //print($id);
        //die();
        $updated = $wpdb->update(
            "{$wpdb->prefix}ksth_ts_geosystem_informations",
            $data,
            ['id' => $id],
            ['%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s'],
            ['%d']
        );
        
        return $updated;
        
    } else {
    
        $inserted = $wpdb->insert(
            "{$wpdb->prefix}ksth_ts_geosystem_informations",
            $data,
            ['%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s']
        );
        
        if ( !$inserted ) {
            return new \WP_Error( 'failed to insert', __( 'Failed to insert data', 'ksth_ts_geosystem' ) );
        }
        
        return $wpdb->insert_id;
    }
}

function ksthTsGetEquipments( $args = [] ) {
    global $wpdb;

    $defalts = ['number' => 20, 'offset' => 0, 'orderby' => 'id', 'order' => 'ASC' ];

    $args = wp_parse_args( $args, $defalts );
    $items = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT * FROM 
             {$wpdb->prefix}ksth_ts_geosystem_informations 
             ORDER BY {$args['orderby']} {$args['order']} 
             LIMIT %d OFFSET %d", $args['number'], $args['offset']
        )
    );

    return $items;
}

function ksthTsGetEquipmentsCount() {
    global $wpdb;
    return (int) $wpdb->get_var( "SELECT count(id) FROM {$wpdb->prefix}ksth_ts_geosystem_informations;" );
}

// Get single Equipment
function ksthTsGeogetEquipment($id) {
    global $wpdb;

    return $wpdb->get_row(
        $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}ksth_ts_geosystem_informations WHERE id=%d", $id )
    );
}

// delete a Equipment
function ksthTsDeleteEquipment( $id ) {
    global $wpdb;

    return $wpdb->delete($wpdb->prefix . 'ksth_ts_geosystem_informations', ['id' => $id], ['%d']);
}

// get single Equipment by tracking and certificate number
function ksthTsGetEquipmentWithTC($trackingNo, $serialNo) {
    global $wpdb;
    // print_r([$trackingNo, $serialNo]);
    // die();
    return $wpdb->get_row(
        $wpdb->prepare(
            "SELECT * FROM {$wpdb->prefix}ksth_ts_geosystem_informations 
             WHERE tracking_no=%s AND serial_number=%s", $trackingNo, $serialNo
        )
    );
}
