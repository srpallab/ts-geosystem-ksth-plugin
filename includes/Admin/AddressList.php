<?php

namespace TSGEOSYSTEM\KSTH\Admin;

if ( !class_exists( 'WP_List_Table' ) ) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

class AddressList extends \WP_List_Table {
    function __construct(){
        parent::__construct([
            'singular' => 'equpment',
            'plural' => 'equpments',
            'ajax' => false
        ]);
    }

    public function get_columns() {
        return [
            'cb' => '<input type="checkbox" />',
            'tracking_no' => __('Tracking No' , 'ksth-ts-geosystem'),
            'equipment' => __('Equipment' , 'ksth-ts-geosystem'),
            'serial_number' => __('Serial Number' , 'ksth-ts-geosystem'),
            'accuracy' => __('Accuracy' , 'ksth-ts-geosystem'),
            'manufacturer' => __('Manufacturer' , 'ksth-ts-geosystem'),
            'company_name' => __('Company Name' , 'ksth-ts-geosystem'),
            'valid_until' => __('Validity' , 'ksth-ts-geosystem'),
        ];
    }

    public function get_sortable_columns() {
        $sortable_columns = [
            'equipment' => ['equipment' , true],
            'serial_number' => ['serial_number', true]
        ];

        return $sortable_columns;
    }

    protected function column_default( $item, $column_name ) {
        switch ( $column_name ) {
        case 'value': {
            break;
        }
        default:
            return isset( $item->$column_name ) ? $item->$column_name : '';
        }
    }

    public function column_equipment( $item ) {
        $actions = [];

        // print_r($item);
        //die();
            
        $actions['edit'] = sprintf(
            '<a href="%s" title="%s">%s</a>',
            admin_url( 'admin.php?page=ksth-ts-geosystem&action=edit&id=' . $item->id ),
            $item->id,
            __( 'Edit', 'ksth-ts-geosystem' ),
            __( 'Edit', 'ksth-ts-geosystem')
        );
        
        $actions['delete'] = sprintf(
            '<a href="%s" class="submitdelete" onclick="return confirm(\'Are you sure?\');" title="%s">%s</a>',
            wp_nonce_url(
                admin_url('admin-post.php?action=ksth-delete-equipment&id=' . $item->id ),
                'ksth-delete-equipment'
            ),
            $item->id,
            __( 'Delete', 'ksth-ts-geosystem'),
             __( 'Delete', 'ksth-ts-geosystem' )
        );
        
        return sprintf(
            '<a href="%1$s"><strong>%2$s</strong></a> %3$s',
            admin_url('admin.php?page=ksth-ts-geosystem&action=view&id=' . $item->id),
            $item->equipment,
            $this->row_actions( $actions )
        );
    }

    protected function column_cb( $item ) {
        return sprintf(
            '<input type="checkbox" name="address_id[]" value="%d" />', $item->id
        );
    }
    
    public function prepare_items() {
        $columns = $this->get_columns();
        $hidden = [];
        $sortable = $this->get_sortable_columns();
        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $offset = ( $currentPage - 1) * $perPage ;
        $args = [
            'number' => $perPage,
            'offset' => $offset
        ];

        if ( isset($_REQUEST['orderby']) && isset($_REQUEST['order'])) {
            $args['orderby'] = $_REQUEST['orderby'];
            $args['order'] = $_REQUEST['order'];
        }
        
        $this->_column_headers = [ $columns, $hidden, $sortable ];
        
        $this->items = ksthTsGetEquipments( $args );
        // print_r($this->items);
        // die();
        $this->set_pagination_args([
            'total_items' => ksthTsGetEquipmentsCount(),
            'per_page' => $perPage
        ]);
    }
}
