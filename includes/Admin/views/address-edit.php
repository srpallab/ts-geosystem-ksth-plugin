<div class="wrap">
  <h1><?php _e( 'Edit Equipment Information', 'ksth-ts-geosystem' )  ?></h1>
  <?php
  if( isset( $_GET['quipment-updateded']) ){ ?>
    <div class="notice notice-success">
      <p> <?php _e('Equipment has been updated successfully!', 'ksth-ts-geosystem') ?> </p>
    </div>
  <?php  }?>
  <form action="" method="post">
    <table class="form-table">
      <tbody>
          <tr class="row <?php echo $this->hasError('trackingNo') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="tracking-no"><?php _e( 'Tracking No', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="tracking-no" type="text" id="tracking-no" class="regular-text"
		   value="<?= esc_attr($quipment->tracking_no) ?>"/>
	    <?php if ($this->hasError('trackingNo')) {?>
	      <p class="description error"><?= $this->getError('trackingNo') ?></p>
	    <?php }?>
	  </td>
	</tr>
          <tr class="row <?php echo $this->hasError('equipment') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="equipment"><?php _e( 'Equipment', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="equipment" type="text" id="equipment" class="regular-text"
		   value="<?= esc_attr($quipment->equipment) ?>" />
	    <?php if ($this->hasError('equipment')) {?>
	      <p class="description error"><?= $this->getError('equipment') ?></p>
	    <?php }?>
	  </td>
	</tr>
        <tr class="row <?php echo $this->hasError('serialNumber') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="serial-number"><?php _e( 'Serial No', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="serial-number" type="text" id="serial-number" class="regular-text"
		   value="<?= esc_attr($quipment->serial_number) ?>" />
	    <?php if ($this->hasError('serialNumber')) {?>
	      <p class="description error"><?= $this->getError('serialNumber') ?></p>
	    <?php }?>
	  </td>
	</tr>
          	<tr class="row <?php echo $this->hasError('accuracy') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="accuracy"><?php _e( 'Accuracy', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="accuracy" type="text" id="accuracy" class="regular-text"
		   value="<?= esc_attr($quipment->accuracy) ?>"/>
	    <?php if ($this->hasError('accuracy')) {?>
	      <p class="description error"><?= $this->getError('accuracy') ?></p>
	    <?php }?>
	  </td>
	</tr>
         
	<tr class="row <?php echo $this->hasError('manufacturer') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="manufacturer"><?php _e( 'Manufacturer', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="manufacturer" type="text" id="manufacturer" class="regular-text"
		   value="<?= esc_attr($quipment->manufacturer) ?>" />
	    <?php if ($this->hasError('manufacturer')) {?>
	      <p class="description error"><?= $this->getError('manufacturer') ?></p>
	    <?php }?>
	  </td>
	</tr>
	<tr class="row <?php echo $this->hasError('companyName') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="company-name"><?php _e( 'Company Name', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="company-name" type="text" id="company-name" class="regular-text"
		   value="<?= esc_attr($quipment->company_name) ?>"/>
	    <?php if ($this->hasError('companyName')) {?>
	      <p class="description error"><?= $this->getError('companyName') ?></p>
	    <?php }?>
	  </td>
	</tr>
 
	<tr class="row <?php echo $this->hasError('validUntil') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="valid-until"><?php _e( 'Valid Until', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="valid-until" type="date" id="valid-until" class="regular-text"
		   value="<?= esc_attr($quipment->valid_until) ?>"/>
	    <?php if ($this->hasError('validUntil')) {?>
	      <p class="description error"><?= $this->getError('validUntil') ?></p>
	    <?php }?>
	  </td>
	</tr>
      </tbody>
    </table>
    <input type="hidden" name="id" value="<?= esc_attr($quipment->id) ?>" />
    <?php wp_nonce_field( 'new-equipment' ); ?>
    <?php submit_button( __('Update Equipment', 'ksth-ts-geosystem'), 'primary', 'submit_equipment' ); ?>
  </form>
</div>
