<div class="wrap">
  <h1><?php _e( 'Add New Equipment', 'ksth-ts-geosystem' )  ?></h1>

 
  <form action="" method="post">
    <table class="form-table">
      <tbody>
    <tr class="row <?php echo $this->hasError('trackingNo') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="tracking-no"><?php _e( 'Tracking No', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="tracking-no" type="text" id="tracking-no" class="regular-text" />
	    <?php if ($this->hasError('trackingNo')) {?>
	      <p class="description error"><?= $this->getError('trackingNo') ?></p>
	    <?php }?>
	  </td>
	</tr>
    <tr class="row <?php echo $this->hasError('equipment') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="equipment"><?php _e( 'Equipment', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="equipment" type="text" id="equipment" class="regular-text" />
	    <?php if ($this->hasError('equipment')) {?>
	      <p class="description error"><?= $this->getError('equipment') ?></p>
	    <?php }?>
	  </td>
	</tr>
        <tr class="row <?php echo $this->hasError('serialNumber') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="serial-number"><?php _e( 'Serial No', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="serial-number" type="text" id="serial-number" class="regular-text" />
	    <?php if ($this->hasError('serialNumber')) {?>
	      <p class="description error"><?= $this->getError('serialNumber') ?></p>
	    <?php }?>
	  </td>
	</tr>
          	
	<tr class="row <?php echo $this->hasError('accuracy') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="accuracy"><?php _e( 'Accuracy', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="accuracy" type="text" id="accuracy" class="regular-text" />
	    <?php if ($this->hasError('accuracy')) {?>
	      <p class="description error"><?= $this->getError('accuracy') ?></p>
	    <?php }?>
	  </td>
	</tr>

	<tr class="row <?php echo $this->hasError('manufacturer') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="manufacturer"><?php _e( 'Manufacturer', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="manufacturer" type="text" id="manufacturer" class="regular-text" />
	    <?php if ($this->hasError('manufacturer')) {?>
	      <p class="description error"><?= $this->getError('manufacturer') ?></p>
	    <?php }?>
	  </td>
	</tr>
	<tr class="row <?php echo $this->hasError('companyName') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="company-name"><?php _e( 'Company Name', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="company-name" type="text" id="company-name" class="regular-text" />
	    <?php if ($this->hasError('companyName')) {?>
	      <p class="description error"><?= $this->getError('companyName') ?></p>
	    <?php }?>
	  </td>
	</tr>
	
	<tr class="row <?php echo $this->hasError('validUntil') ? ' form-invalid form-required' : '' ?>">
	  <th class="row">
	    <label for="valid-until"><?php _e( 'Validity', 'ksth-ts-geosystem' ) ?></label>
	  </th>
	  <td>
	    <input name="valid-until" type="date" id="valid-until" class="regular-text" />
	    <?php if ($this->hasError('validUntil')) {?>
	      <p class="description error"><?= $this->getError('validUntil') ?></p>
	    <?php }?>
	  </td>
	</tr>
      </tbody>
    </table>
    <?php wp_nonce_field( 'new-equipment' ); ?>
    <?php submit_button( __('Add New Equipment', 'ksth-ts-geosystem'), 'primary', 'submit_equipment' ); ?>
  </form>
</div>
