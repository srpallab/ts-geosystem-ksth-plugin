<div class="wrap">
    <h1 class="wp-heading-inline" ><?php _e( 'Equpments List', 'ksth-ts-geosystem' )  ?></h1>

    <a href="<?php echo admin_url('admin.php?page=ksth-ts-geosystem&action=new') ?>"
       class="page-title-action">
      <?php _e( 'Add New', 'ksth-address-book' ) ?>
    </a>
    <?php if( isset( $_GET['inserted']) ){ ?>
      <div class="notice notice-success">
        <p> <?php _e('Equipment has been added successfully!', 'ksth-ts-geosystem') ?> </p>
      </div>
    <?php  }?>
    <?php if( isset( $_GET['equipment-delete']) ){ ?>
      <?php if($_GET['equipment-delete']  == true) { ?>
	<div class="notice notice-success">
          <p> <?php _e('Equipment has been delete successfully!', 'ksth-ts-geosystem') ?> </p>
	</div>
      <?php  } else { ?>
	<div class="notice notice-failed">
          <p> <?php _e('Equipment delete Failed!', 'ksth-ts-geosystem') ?> </p>
	</div>
      <?php } ?>
    <?php } ?>
    
    <form action="" method="post">
      <?php
      $table = new TSGEOSYSTEM\KSTH\Admin\AddressList();
      $table->prepare_items();
      $table->display();
      ?>
    </form>
</div>
