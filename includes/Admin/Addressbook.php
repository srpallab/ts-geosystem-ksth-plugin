<?php
namespace TSGEOSYSTEM\KSTH\Admin;

use TSGEOSYSTEM\KSTH\Traits\Form_Error;

class Addressbook {
    
    use Form_Error;
    
    public function pluginPage() {
        $action = isset($_GET['action']) ?  $_GET['action'] : 'list';
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        
        switch ($action) {
        case 'new': {
            $template = __DIR__ . '/views/address-new.php';
            break;
        }
        case 'edit': {
            $quipment = ksthTsGeogetEquipment($id);
            $template = __DIR__ . '/views/address-edit.php';
            break;
        }
        case 'view': {
            $template = __DIR__ . '/views/address-view.php';
            break;
        }
        default:
            $template = __DIR__ . '/views/address-list.php';
            break;
        }

        if ( file_exists( $template )) {
            include $template;
        }
    }


    public function formHandler() {
        if ( !isset( $_POST['submit_equipment'] ) ) {
            return;
        }

        if ( !wp_verify_nonce( $_POST['_wpnonce'], 'new-equipment' )) {
            wp_die( 'Are you cheating on nonce?' );
        }

        if ( !current_user_can( 'manage_options' ) ) {
            wp_die( 'Are you cheating?' );
        }

        $id = isset( $_POST['id'] ) ? intval( $_POST['id'] ) : 0 ;
        $trackingNo = isset( $_POST['tracking-no'] ) ? sanitize_text_field( $_POST['tracking-no'] ) :  '' ;
        $equipment = isset( $_POST['equipment'] ) ? sanitize_text_field( $_POST['equipment'] ) :  '' ;
        $serialNumber = isset( $_POST['serial-number'] ) ? sanitize_text_field( $_POST['serial-number'] ) :  '' ;
        $accuracy = isset( $_POST['accuracy'] ) ? sanitize_text_field( $_POST['accuracy'] ) :  '' ;
        $manufacturer = isset( $_POST['manufacturer'] ) ? sanitize_text_field( $_POST['manufacturer'] ) :  '' ;
        $companyName = isset( $_POST['company-name'] ) ? sanitize_text_field( $_POST['company-name'] ) :  '' ;
        $validUntil = isset( $_POST['valid-until'] ) ? sanitize_text_field( $_POST['valid-until'] ) :  '' ;


        if (empty($trackingNo)) {
            $this->errors['trackingNo'] = 'Please Provide a Tracking No';
        }
        if (empty($equipment)) {
            $this->errors['equipment'] = 'Please Provide a Equipment';
        }
        if (empty($serialNumber)) {
            $this->errors['serialNumber'] = 'Please Provide a Serial Number';
        }
        if (empty($accuracy)) {
            $this->errors['accuracy'] = 'Please Provide a Accuracy';
        }
        if (empty($manufacturer)) {
            $this->errors['manufacturer'] = 'Please Provide a Manufacturer';
        }
        if (empty($companyName)) {
            $this->errors['companyName'] = 'Please Provide a Company Name';
        }
        if (empty($validUntil)) {
            $this->errors['validUntil'] = 'Please Provide a Valid Date';
        }
        if ( !empty( $this->errors ) ) {
            return;
        }
        $args = [
            'tracking_no' => $trackingNo,
            'equipment' => $equipment,
            'serial_number' => $serialNumber,
            'accuracy' => $accuracy,
            'manufacturer' => $manufacturer,
            'company_name' => $companyName,
            'valid_until' => $validUntil
        ];
        if ($id) {
            // print_r([$args, $id]);
            // die();
            $args['id'] = $id;
        }
        
        $insert_id = ksthAcInsertAddress( $args );

        if ( is_wp_error( $insert_id ) ) {
            wp_die( $insert_id->get_error_message() );
        }
        if ($id) {
            $redirected_to = admin_url(
                'admin.php?page=ksth-ts-geosystem&action=edit&quipment-updateded=true&id=' . $id
            );
        } else{
            $redirected_to = admin_url('admin.php?page=ksth-ts-geosystem&inserted=true');
        }
        
        wp_redirect($redirected_to);
        exit;
    }

    public function deleteEquipment() {
        if ( !wp_verify_nonce( $_GET['_wpnonce'], 'ksth-delete-equipment' )) {
            wp_die( 'Are you cheating on nonce?' );
        }

        if ( !current_user_can( 'manage_options' ) ) {
            wp_die( 'Are you cheating?' );
        }

        $id = isset( $_GET['id'] ) ? intval( $_GET['id'] ) : 0 ;
        
        if (ksthTsDeleteEquipment($id)) {
            $redirected_to = admin_url('admin.php?page=ksth-ts-geosystem&equipment-delete=true');
        } else{
            $redirected_to = admin_url('admin.php?page=ksth-ts-geosystem&equipment-delete=false');
        }
        
        wp_redirect($redirected_to);
        exit;
    }
}
