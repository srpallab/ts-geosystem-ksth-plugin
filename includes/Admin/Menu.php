<?php

namespace TSGEOSYSTEM\KSTH\Admin;

class Menu {
    public $addressBookObj;

    
    function __construct($addressBookObj){
        $this->addressBookObj = $addressBookObj;
        // die();
        add_action( 'admin_menu', [$this, 'adminMenu'] );
    }

    public function adminMenu() {
        $menu_slug = 'ksth-ts-geosystem';
        $capability = 'manage_options';
        $hook = add_menu_page(
            __('TS Geosystem', 'ksth-ts-geosystem'),
            __('TS CC', 'ksth-ts-geosystem'),
            $capability,
            $menu_slug,
            [$this->addressBookObj, 'pluginPage'],
            'dashicons-book-alt'
        );

        add_action('admin_head-' . $hook, [ $this, 'enqueueAssets'] );
    }


    public function enqueueAssets() {
        // wp_enqueue_script('ts-geosystem-script');
        wp_enqueue_style('ts-geosystem-style');
    }
}
