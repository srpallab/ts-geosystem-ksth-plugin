<?php
namespace TSGEOSYSTEM\KSTH\Frontend;

class Shortcode {
    
    function __construct(){
        add_shortcode( 'ts-geosystem-ksth-tracking', [$this, 'renderShortcode'] );
    }

    public function renderShortcode( $attr, $content = '' ) {
        wp_enqueue_script('ts-geosystem-script');
        wp_enqueue_style('ts-geosystem-style');

        ob_start();
        include __DIR__ . '/views/tracking.php';
        
        return ob_get_clean();
    }
}
