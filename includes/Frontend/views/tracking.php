<div class="ts-geosystem-ksth-tracking" id="ts-geosystem-ksth-tracking-from">
  <form action="" method="post" class="flex-ksth justify-evenly-ksth items-center-ksth">
    <div class="form-row display-inline text-white-ksth flex-ksth items-center-ksth ksth-width-full">
      <!-- <label for="tracking-number" class="font-2xl-ksth">Tracking Number</label> -->
      <input id="tracking-number" name="tracking-number" 
	     type="text" class="input-ksth ksth-width-95" placeholder="Tracking Number" />
    </div>
    <div class="form-row display-inline text-white-ksth flex-ksth items-center-ksth ksth-width-full">
      <!-- <label for="serial-number" class="font-2xl-ksth">Equipment Serial Number</label> -->
      <input id="serial-number" name="serial-number" 
	     type="text" class="input-ksth ksth-width-95" placeholder="Equipment Serial Number" />
    </div>

    <div class="form-row display-inline">
      <?php wp_nonce_field('wp-ac-enquiry-form'); ?>
      <input name="action" type="hidden" value="ts_geosystem_ksth_tracking" />
      <input name="send_tracking" type="submit" class="submit-button-ksth"
	     value="<?php esc_attr_e( 'SUBMIT', 'ts-geosystem-ksth-tracking' ) ?>"/>
    </div>
  </form>
  <div id="ts-geosystem-ksth-data-show" class="py-5-ksth text-white-ksth"></div>
</div>
